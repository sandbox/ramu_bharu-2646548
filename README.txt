CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration

INTRODUCTION
------------
This module used to run migrate classes automatically in cron job.

REQUIREMENTS
------------
Install migrate Module. https://www.drupal.org/project/migrate
It will work with migrate module.

RECOMMENDED MODULES
------------
Install migrate module

INSTALLATION
------------
1. Check requirements section first.
2. Enable the module.
https://www.drupal.org/documentation/install/modules-themes/modules-7

CONFIGURATION
------------
To run the migrate classes with Cron run, simply visit the configuration page of rmc module
and assign the required migrate classes to the Cron job.
Settings page for migrate classes at
admin/config/migration_classes

